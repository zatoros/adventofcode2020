#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <regex>
#include <vector>
#include <boost/algorithm/string.hpp>


class Range
{
public:
	Range(int lower, int upper) :
		_lower(lower), _upper(upper)
	{ }

	bool IsInRange(int value) const
	{
		return value >= _lower &&
			value <= _upper;
	}

private:
	int _lower;
	int _upper;
};

class Field
{
public:
	Field(int id, Range range1, Range range2) :
		_id(id), _range1(range1), _range2(range2)
	{ }

	bool IsInRange(int value) const
	{
		return _range1.IsInRange(value) ||
			_range2.IsInRange(value);
	}

	int GetId() const
	{
		return _id;
	}

	bool operator<(const Field& other) const
	{
		return _id < other._id;
	}

private:
	int _id;
	Range _range1;
	Range _range2;
};

class Ticket
{
public:
	Ticket(const std::string& valuesInput)
	{
		std::vector<std::string> valuesStr;
		boost::split(valuesStr, valuesInput, boost::is_any_of(","));
		std::transform(valuesStr.begin(), valuesStr.end(),
			std::back_inserter(_values),
			[](const std::string& valueStr) { return std::stoi(valueStr); });
	}

	int GetSumOfInvalidFields(const std::vector<Field>& fields) const
	{
		return std::accumulate(_values.begin(), _values.end(), 0,
			[&fields](int sum, int value)
		{
			if (std::none_of(fields.begin(), fields.end(),
				[value](const Field& field)
			{
				return field.IsInRange(value);
			}))
				return sum + value;
			return sum;
		});
	}

	int GetField(int index) const
	{
		return _values[index];
	}

private:
	std::vector<int> _values;
};

class FieldMapper
{
public:
	FieldMapper(const std::vector<Field>& fields) : _mapping(fields.size()), _fields(fields)
	{ }

	void InitializeMapping(const Ticket& yourTicket)
	{
		int fieldsSize = _fields.size();
		for (int valueIndex = 0; valueIndex < fieldsSize; valueIndex++)
		{
			int value = yourTicket.GetField(valueIndex);
			auto& fieldMapping = _mapping[valueIndex];
			for (const auto& field : _fields)
			{
				if (field.IsInRange(value))
				{
					fieldMapping.push_back(&field);
				}
			}
		}
	}

	void FilterMapping(const std::vector<Ticket>& tickets)
	{
		int fieldsSize = _fields.size();
		for (const auto& ticket : tickets)
		{
			for (int valueIndex = 0; valueIndex < fieldsSize; valueIndex++)
			{
				int value = ticket.GetField(valueIndex);
				auto& fieldMapping = _mapping[valueIndex];
				for (auto fieldMappingIt = fieldMapping.begin(); fieldMappingIt != fieldMapping.end();)
				{
					if (!(*fieldMappingIt)->IsInRange(value))
					{
						fieldMappingIt = fieldMapping.erase(fieldMappingIt);
					}
					else
					{
						fieldMappingIt++;
					}
				}
			}
		}
	}

	void ResolveMapping()
	{
		std::list<const Field*> result;
		resolve(result);
		_result.insert(_result.begin(), result.begin(), result.end());
	}

	unsigned long long GetResult(const Ticket& yourTicket) const
	{
		unsigned long long result{ 1 };
		for (size_t index = 0; index < _fields.size(); index++)
		{
			if (_result[index]->GetId() >= 0 &&
				_result[index]->GetId() < 6)
			{
				result *= yourTicket.GetField(index);
			}
		}
		return result;
	}

private:
	void resolve(std::list<const Field*>& result)
	{
		size_t level = result.size();
		if (result.size() == _fields.size())
			return;

		const auto& fieldMapping = _mapping[level];
		for (const auto& mapping : fieldMapping)
		{
			if (std::find(result.begin(), result.end(), mapping) == result.end())
			{
				result.push_back(mapping);
				resolve(result);
				if (result.size() == _fields.size())
					break;
				result.pop_back();
			}
		}
	}

	std::vector<std::list<const Field*>> _mapping;
	const std::vector<Field>& _fields;
	std::vector<const Field*> _result;
};

int main(void)
{
	std::string input;

	const std::regex fieldRegex(R"***(.+: (\d+)-(\d+) or (\d+)-(\d+))***");
	int id{ 0 };
	std::vector<Field> fields;
	while (std::getline(std::cin, input) &&
		!input.empty())
	{
		std::smatch matches;
		if (std::regex_search(input, matches, fieldRegex))
		{
			fields.emplace_back(id++,
				Range(std::stoi(matches[1].str()), std::stoi(matches[2].str())),
				Range(std::stoi(matches[3].str()), std::stoi(matches[4].str())));
		}
	}

	std::getline(std::cin, input);
	std::getline(std::cin, input);

	Ticket yourTicker(input);

	std::getline(std::cin, input);
	std::getline(std::cin, input);

	std::vector<Ticket> tickets;
	while (std::getline(std::cin, input))
	{
		tickets.emplace_back(input);
	}

	int result1{ 0 };
	for (auto ticketIt = tickets.begin(); ticketIt != tickets.end(); )
	{
		int sum = ticketIt->GetSumOfInvalidFields(fields);
		if (sum > 0)
		{
			result1 += sum;
			ticketIt = tickets.erase(ticketIt);
		}
		else
		{
			ticketIt++;
		}
	}
	std::cout << result1 << std::endl;

	FieldMapper fieldMapper(fields);
	fieldMapper.InitializeMapping(yourTicker);
	fieldMapper.FilterMapping(tickets);
	fieldMapper.ResolveMapping();
	std::cout << fieldMapper.GetResult(yourTicker) << std::endl;

	return 0;
}