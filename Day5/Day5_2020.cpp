#include <algorithm>
#include <climits>
#include <deque>
#include <iostream>
#include <string>
#include <set>

int getPartId(const std::string& seatString, int index, int min, int max)
{
    if(min == max)
        return min;
    int mid = (max - min + 1) / 2;
    if( seatString[index] == 'B' ||
        seatString[index] == 'R' )
    {
        min = max - mid + 1;
    }
    else
    {
        max = mid + min - 1;
    }
    return getPartId(seatString, index + 1, min, max);
}

int getId(const std::string& seatString)
{
    if(seatString.size() != 10)
        return 0;
    
    return getPartId(seatString, 0, 0, 127) * 8 + getPartId(seatString, 7, 0, 7);
}

int main(void)
{
    std::string seatInput;
    std::set<int> results;
    while(std::getline(std::cin, seatInput))
    {
        results.insert(getId(seatInput));
    }
    
    int result = INT_MIN;
    for(const int seatId : results)
    {
        if(result == INT_MIN)
            result = seatId;
        else
        {
            result = result + 1;
            if(result != seatId)
                break;
        }
    }
    
    std::cout << *results.rbegin() << std::endl; // Task 1
    std::cout << result << std::endl; // Task 2
    return 0;
}