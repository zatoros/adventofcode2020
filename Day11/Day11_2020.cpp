#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <utility>

enum Position : char
{
	FLOOR = '.',
	EMPTY = 'L',
	OCCUPIED = '#'
};

using Direction = std::pair<int, int>;
using MapRow = std::map<int, Position >;
using Map = std::map<int, MapRow>;

class CellularAutomation
{
public:
	CellularAutomation(const Map& map) : _map(map)
	{
	}

	void Run(int part)
	{
		Map nextStep;
		do
		{
			for (const auto& [row, mapRow] : _map)
			{
				for (const auto& [col, seat]: mapRow)
				{
					nextStep[row][col] = applyRule(row, col, part);
				}
			}
			_map.swap(nextStep);
			//output();
		} while (nextStep != _map);
	}

	int GetOccupiedSeats() const
	{
		int result{ 0 };
		std::for_each(_map.begin(), _map.end(),
			[&result](Map::const_reference mapRow)
		{
			result += std::count_if(mapRow.second.begin(), mapRow.second.end(),
				[](MapRow::const_reference seat)
			{
				return seat.second == Position::OCCUPIED; 
			});
		}
		);
		return result;
	}
private:
	Position applyRule(int row, int col, int part) const
	{
		char pos = _map.at(row).at(col);
		if (pos == Position::FLOOR)
		{
			return Position::FLOOR;
		}
		else
		{
			int adj = countAdjOccupiedSeats(row, col, part);
			if (pos == Position::OCCUPIED &&
				adj >= (part == 1 ? 4 : 5))
			{
				return Position::EMPTY;
			}
			else if (pos == Position::EMPTY && 
				adj == 0)
			{
				return Position::OCCUPIED;
			}
		}
	}


	int countAdjOccupiedSeats(int row, int col, int part) const
	{
		Direction directions[] = {
			{-1,-1}, {-1, 0}, {-1,+1},
		    {0, -1}, {0, +1},
		    {1, -1}, {1, 0}, {1, 1}
		};

		return std::count_if(directions, directions + 8,
			[part, this, row, col](const Direction& dir) {return this->isOccupied(dir, row, col, part); });
	}

	bool isOccupied(const Direction& direction,int row, int col, int part) const
	{
		do
		{
			row += direction.first;
			col += direction.second;
			if (isOutOfBorder(row, col))
			{
				return false;
			}
		} while (part == 2 && _map.at(row).at(col) == Position::FLOOR);

		return _map.at(row).at(col) == Position::OCCUPIED;
	}

	bool isOutOfBorder(int row, int col) const
	{
		if (row < 0 || row >= _map.size() ||
			col < 0 || col >= _map.begin()->second.size())
		{
			return true;
		}
		return false;
	}

	void output() const
	{
		for (const auto&[row, mapRow] : _map)
		{
			for (const auto&[col, seat] : mapRow)
			{
				std::cout << static_cast<char>(_map.at(row).at(col));
			}
			std::cout << std::endl;
		}
		std::cout << "----------------------\n";
	}

	Map _map;
};

int main()
{
	Map map;

	std::string input;
	int row{ 0 }, col;
	while (std::getline(std::cin, input))
	{
		col = 0;
		for (const char& inputPos : input)
		{
			map[row][col++] = static_cast<Position>(inputPos);
		}
		row++;
	}

	CellularAutomation ca(map);
	//ca.Run(1);
	//std::cout << ca.GetOccupiedSeats() << std::endl; //Task 1;
	ca.Run(2);
	std::cout << ca.GetOccupiedSeats() << std::endl; //Task 2;
	return 0;
}