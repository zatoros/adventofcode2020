#include <iostream>
#include <vector>

int main()
{
	std::vector<int> inputs;
	for (int input; std::cin >> input;)
	{
		inputs.push_back(input);
	}

	int result1 = 0;
	int result2 = 0;
	for (size_t index1 = 0; index1 < inputs.size(); index1++)
	{
		for (size_t index2 = index1 + 1; index2 < inputs.size(); index2++)
		{
			if (result1 == 0)
			{
				if (inputs[index1] + inputs[index2] == 2020)
				{
					result1 = inputs[index1] * inputs[index2];
				}
			}
			if (result2 != 0)
				continue;
			for (size_t index3 = index2 + 1; index3 < inputs.size(); index3++)
			{
				if (inputs[index1] + inputs[index2] + inputs[index3] == 2020)
				{
					result2 = inputs[index1] * inputs[index2] * inputs[index3];
					break;
				}
			}
			if (result1 != 0 && result2 != 0)
				break;
		}
		if (result1 != 0 && result2 != 0)
			break;
	}

	std::cout << result1 << std::endl;
	std::cout << result2 << std::endl;
}