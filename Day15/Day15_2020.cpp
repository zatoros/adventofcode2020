#include <iostream>
#include <map>

int main(void)
{
	unsigned long turn{ 0 };
	std::map<unsigned long, std::pair<unsigned long, unsigned long>> numbers;
	unsigned long last;
	unsigned long input;
	while (std::cin >> input)
	{
		turn++;
		numbers[input] = { 0, turn };
		last = input;
		std::cin.get();
	}
	while (turn != 30000000)
	{
		turn++;
		const auto&[age0Last, age1Last] = numbers[last];
		if (age0Last == 0)
		{
			last = 0;
		}
		else
		{
			last = age1Last - age0Last;
		}

		auto&[age0New, age1New] = numbers[last];
		age0New = age1New;
		age1New = turn;
	}
	std::cout << last << std::endl;
	return 0;
}