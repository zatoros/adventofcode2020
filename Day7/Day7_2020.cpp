#include <iostream>
#include <list>
#include <map>
#include <regex>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using Color = std::string;
struct Node
{
	Color parent;
	Color child;
	int weight;
};
struct LevelNode
{
	Node node;
	int level;

	bool operator<(const LevelNode& other) const
	{
		if (level != other.level)
			return level > other.level;
		if (node.parent != other.node.parent)
			return node.parent < other.node.parent;
		return node.child < other.node.child;
	}
};
const std::string NO_BAGS{ "no other" };
const std::string SHINY_GOLD{ "shiny gold" };

void findConnectedNodes(std::list<Node>& nodesToProcess, std::set<LevelNode>& connectedNodes, const std::string& parent,
	int level = 0)
{
	for (auto it = nodesToProcess.begin(); it != nodesToProcess.end(); it++)
	{
		if (it->parent == parent)
		{
			connectedNodes.insert(LevelNode{ *it, level });
			findConnectedNodes(nodesToProcess, connectedNodes, it->child, level + 1);
		}
	}
}

void getColorValues(std::set<LevelNode>& connectedNodes, std::map<Color, int>& colorValues)
{
	for (auto nodeIt = connectedNodes.begin(); nodeIt != connectedNodes.end(); nodeIt++)
	{
		const Color& processedParent = nodeIt->node.parent;
		int parentValue{ 0 };
		while (nodeIt != connectedNodes.end() && 
			processedParent == nodeIt->node.parent )
		{
			int childValue = colorValues[nodeIt->node.child];
			parentValue += nodeIt->node.weight *
				(childValue != 0 ? childValue : 1);
			nodeIt++;
		}
		nodeIt--;
		colorValues[processedParent] = parentValue + 1;
	}
}

int main(void)
{
	std::regex bag_regex("(\\d+ )?(\\w+ \\w+) bags?");
	std::string input;
	std::vector<Node> nodes;
	while (std::getline(std::cin, input))
	{
		auto bags_begin = std::sregex_iterator(input.begin(), input.end(), bag_regex);
		auto bags_end = std::sregex_iterator();
		Color parent;
		for (std::sregex_iterator bagsIt = bags_begin; bagsIt != bags_end; ++bagsIt)
		{
			std::smatch bagMatch = *bagsIt;
			if (bagsIt->size() == 3)
			{
				Color col = (*bagsIt)[2].str();
				if (parent.empty())
					parent.swap(col);
				else if (col != NO_BAGS)
				{
					int weight = std::stoi((*bagsIt)[1].str());
					nodes.emplace_back(Node{ parent, col, weight });
				}
			}
		}
	}

	std::set<Color> result;
	{
		std::list<Node> nodesToProcess(nodes.begin(), nodes.end());
		int nodesProcessed{ 0 };
		result.insert(SHINY_GOLD);
		do
		{
			nodesProcessed = 0;
			for (auto it = nodesToProcess.begin(); it != nodesToProcess.end(); it++)
			{
				if (result.find(it->child) != result.end())
				{
					result.insert(it->parent);
					it = nodesToProcess.erase(it);
					nodesProcessed++;
				}
			}
		} while (nodesProcessed != 0);
	}

	std::map<Color, int> colorValues;
	{
		std::list<Node> nodesToProcess(nodes.begin(), nodes.end());
		std::set<LevelNode> connectedNodes;
		findConnectedNodes(nodesToProcess, connectedNodes, SHINY_GOLD);
		getColorValues(connectedNodes, colorValues);
	}

	std::cout << result.size() - 1 << std::endl; // Task 1
	std::cout << colorValues[SHINY_GOLD] - 1 << std::endl; // Task 2
	return 0;
}