#include <iostream>
#include <regex>
#include <string>
#include <map>
#include <numeric>
#include <list>
#include <vector>
#include <boost/multiprecision/cpp_int.hpp>

using MemType = unsigned long long;
using MemIndexType = unsigned;
const MemType MEM_MASK = 0xFFFFFFFFF;
const MemIndexType MEM_SIZE = 36;
using OutputType = boost::multiprecision::uint256_t;
using ProgMemType = std::map<MemType, MemType>;

OutputType calcMem(const ProgMemType& mem)
{
    return std::accumulate(mem.begin(),
                                 mem.end(), 
                                 OutputType(0), 
                                 [](OutputType sum, ProgMemType::const_reference m2)
                                 { return sum + m2.second; });
}

std::list<MemType> generateMem2Adresses(MemType value, const std::vector<MemIndexType>& maskX)
{
    std::list<MemType> result;
    
    if(maskX.empty())
    {
        result.push_back(value);
        return result;
    }
    
    MemType permutations = 1 << maskX.size();
    for(MemType permutation = 0; permutation < permutations; permutation++)
    {
        for(MemIndexType bit = 0; bit < maskX.size(); bit++)
        {
            MemIndexType bitValue = (static_cast<MemType>(1) << bit) & permutation;
            if(bitValue)
            {
                value |= (static_cast<MemType>(1) << maskX[bit]);
            }
            else
            {
                value &= ~(static_cast<MemType>(1) << maskX[bit]);
            }
        }
        result.push_back(value);
    }
    return result;
}

int main(void)
{
    const std::regex maskRegex(R"***(mask = ([X\d]{36}))***");
    const std::regex memRegex(R"***(mem\[(\d+)\] = (\d+))***");
    
    ProgMemType mem, mem2;
    MemType mask0 = ~0;
    MemType mask1 = 0;
    std::vector<MemIndexType> maskX;
    
    std::string input;
    while(std::getline(std::cin, input))
    {
        std::smatch matches;
        if(std::regex_search(input, matches, maskRegex))
        {
            std::string mask(matches[1].str());
            
            mask0 = ~0;
            mask1 = 0;            
            maskX.clear();
            
            for(MemIndexType i = 0; i < MEM_SIZE; i++)
            {
                MemIndexType bitIndex = MEM_SIZE - 1 - i;
                
                if(mask[i] == 'X')
                {
                    maskX.push_back(bitIndex);
                }
                
                MemType bit = static_cast<MemType>(1) << bitIndex;
                if(mask[i] == '0')
                {
                    mask0 &= ~(bit);
                }
                else if(mask[i] == '1')
                {
                    mask1 |= bit;
                }
            }
        }
        else if(std::regex_search(input, matches, memRegex))
        {
            MemType index = std::stoi(matches[1].str());
            MemType value = std::stoi(matches[2].str());
            
            mem[index] = ((value & mask0) | mask1) & MEM_MASK;
            
            for(const auto& adress : generateMem2Adresses((index | mask1) & MEM_MASK, maskX))
                mem2[adress] = value;            
        }
    }
    
    std::cout << calcMem(mem) << std::endl; // Task 1    
    std::cout << calcMem(mem2) << std::endl; // Task 2
    
    return 0;
}