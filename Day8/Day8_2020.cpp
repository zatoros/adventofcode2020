#include <iostream>
#include <sstream>
#include <string>
#include <set>
#include <vector>

using Code = std::string;
static const Code ACC = "acc";
static const Code JMP = "jmp";
static const Code NOP = "nop";

using Argument = int;

struct Instruction
{
	Code code;
	Argument argument;

	Instruction(const std::string& input)
	{
		std::istringstream in(input);
		in >> code >> argument;
	}
};

class Program
{
public:
	Program() = default;

	void AddInstruction(const Instruction& inst)
	{
		instructions.push_back(inst);
		instructionsBackUp.push_back(inst);
	}

	void Execute()
	{
		std::set<int> linesExecuted;
		while (linesExecuted.find(pointer) == linesExecuted.end() &&
			pointer < instructions.size())
		{
			linesExecuted.insert(pointer);
			const Code& code = instructions[pointer].code;
			if (code == ACC)
			{
				accumulator += instructions[pointer].argument;
				pointer++;
			}
			else if (code == JMP)
			{
				pointer += instructions[pointer].argument;
			}
			else if (code == NOP)
			{
				pointer++;
			}
		}
		noLoopFlag = (pointer >= instructions.size());
	}

	size_t TryFix(size_t changeIndex)
	{
		while (changeIndex < instructions.size())
		{
			if (instructions[changeIndex].code == NOP)
			{
				instructions[changeIndex++].code = JMP;
				break;
			}
			if (instructions[changeIndex].code == JMP)
			{
				instructions[changeIndex++].code = NOP;
				break;
			}
			changeIndex++;
		}
		return changeIndex;
	}

	void Reset()
	{
		noLoopFlag = false;
		accumulator = pointer = 0;
		instructions = instructionsBackUp;
	}

	int GetAccumulator() const
	{
		return accumulator;
	}

	bool GetNoLoopFlag() const
	{
		return noLoopFlag;
	}
private:
	bool noLoopFlag{ false };
	int accumulator{ 0 };
	size_t pointer{ 0 };
	std::vector<Instruction> instructions;
	std::vector<Instruction> instructionsBackUp;
};
int main()
{
	Program program;
	std::string input;
	while (std::getline(std::cin, input))
	{
		program.AddInstruction(input);
	}
	program.Execute();
    std::cout << program.GetAccumulator() << std::endl; // task 1

	size_t changeIndex{ 0 };
	do
	{
		program.Reset();
		changeIndex = program.TryFix(changeIndex);
		program.Execute();
	} while (!program.GetNoLoopFlag());
	std::cout << program.GetAccumulator() << std::endl; // task 2
}