#include <cmath>
#include <iostream>
#include <utility>
#include <vector>

enum Action : char
{
    NORTH = 'N',
    SOUTH = 'S',
    WEAST = 'W',
    EAST = 'E',
    LEFT = 'L',
    RIGHT = 'R',
    FORWARD = 'F'
};

enum Direction : char
{
    NORTH_DIR = 'N',
    SOUTH_DIR = 'S',
    WEAST_DIR = 'W',
    EAST_DIR = 'E',
};

struct Instruction
{
    Action action;
    int value;
};

class Ship
{
public:
    void ProcessInstruction(Instruction inst)
    {
        switch(inst.action)
        {
            case Action::NORTH:
                moveDirection(Direction::NORTH_DIR, inst.value);
                moveWayPoint(Direction::NORTH_DIR, inst.value);
                break;
            case Action::SOUTH:
                moveDirection(Direction::SOUTH_DIR, inst.value);
                moveWayPoint(Direction::SOUTH_DIR, inst.value);
                break;
            case Action::WEAST:
                moveDirection(Direction::WEAST_DIR, inst.value);
                moveWayPoint(Direction::WEAST_DIR, inst.value);
                break;
            case Action::EAST:
                moveDirection(Direction::EAST_DIR, inst.value);
                moveWayPoint(Direction::EAST_DIR, inst.value);
                break;
            case Action::LEFT:               
                rotate(inst.value);
                rotateWayPoint(inst.value);
                break;
            case Action::RIGHT:
                rotate(-inst.value);
                rotateWayPoint(-inst.value);
                break;
            case Action::FORWARD:
                moveDirection(direction, inst.value);
                moveShip(inst.value);
                break;
        }
        //std::cout << "east " << east << " north " << north << " " << static_cast<char>(direction) <<std::endl;
        //std::cout << "waypointEast " << waypointEast << " waypointNorth " << waypointNorth << " " << static_cast<char>(inst.action) << inst.value << std::endl;
        //std::cout << "shipEast " << shipEast << " shipNorth " << shipNorth << std::endl;
    }
    
    int GetDistance() const
    {
        return std::abs(east) + std::abs(north);
    }
    
    int GetDistanceShip() const
    {
        return std::abs(shipEast) + std::abs(shipNorth);
    }
private:
    void moveDirection(Direction dir, int value)
    {
        switch(dir)
        {
            case Direction::NORTH_DIR:
                north += value;
                break;
            case Direction::SOUTH_DIR:
                north -= value;
                break;
            case Direction::WEAST_DIR:
                east -= value;
                break;
            case Direction::EAST_DIR:
                east += value;
                break;   
        }
    }
    
    void rotate(int degree)
    {
        int steps = std::abs(degree) / 90;
        
        static std::vector<Direction> dirs{EAST_DIR, NORTH_DIR, WEAST_DIR, SOUTH_DIR};
        auto it = std::find(dirs.begin(), dirs.end(), direction);
        for(int i = 0; i < steps; i++)
        {
            if(degree > 0)
            {
                if(++it == dirs.end())
                {
                    it = dirs.begin();
                }
            }
            else
            {
                if(it == dirs.begin())
                    it = dirs.end();
                --it;
            }
        }
        direction = *it;
    }
    
    void moveWayPoint(Direction dir, int value)
    {
        switch(dir)
        {
            case Direction::NORTH_DIR:
                waypointNorth += value;
                break;
            case Direction::SOUTH_DIR:
                waypointNorth -= value;
                break;
            case Direction::WEAST_DIR:
                waypointEast -= value;
                break;
            case Direction::EAST_DIR:
                waypointEast += value;
                break;   
        }
    }
    
    void rotateWayPoint(int degree)
    {
        static std::vector<std::pair<int, int>> sinCosTable{{0,1}, {1,0}, {0,-1}, {-1,0}};
        int steps = (std::abs(degree) / 90) % 4;
        const auto[sin, cos] = sinCosTable[steps];
        
        int waypointEastTemp = waypointEast, waypointNorthTemp = waypointNorth;;
        if(degree > 0)
        {
            waypointEast =  waypointEastTemp*cos - waypointNorthTemp*sin;
            waypointNorth = waypointEastTemp*sin + waypointNorthTemp*cos;
        }
        else
        {
            waypointEast =  waypointEastTemp*cos + waypointNorthTemp*sin;
            waypointNorth = -waypointEastTemp*sin + waypointNorthTemp*cos;
        }
    }
    
    void moveShip(int value)
    {
        shipEast += waypointEast*value;
        shipNorth += waypointNorth*value;
    }
    
    //Task 1
    Direction direction{EAST_DIR};
    int east{0};
    int north{0};
    
    //Task2
    int waypointEast{10};
    int waypointNorth{1};
    int shipEast{0};
    int shipNorth{0};
};

int main(void)
{   
    Ship ship;
    
    char inputAct;
    int inputVal;
    while(std::cin >> inputAct >> inputVal)
    {
        ship.ProcessInstruction({static_cast<Action>(inputAct), inputVal});
    }
    std::cout << ship.GetDistance() << std::endl; //Task 1
    std::cout << ship.GetDistanceShip() << std::endl; //Task 2
}