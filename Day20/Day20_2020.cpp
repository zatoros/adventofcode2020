#include <algorithm>
#include <iostream>
#include <map>
#include <regex>
#include <list>
#include <queue>
#include <string>
#include <utility>
#include <vector>

static const size_t PUZZLE_SIZE{10};

class Puzzle
{
public:
    static const size_t EDES_SIZE{4};
    
    enum TILE : char
    {
        DOT = '.',
        HASH = '#'
    };
    
    using Edge = std::vector<TILE>;
    
    Puzzle() : _id(0)
    {
    }
    
    Puzzle(int id, const std::vector<std::string>& puzzleData) : _id(id)
    {
        for(auto it = puzzleData.front().cbegin(); it != puzzleData.front().cend(); it++)
            _edges[0].push_back(static_cast<TILE>(*it));
        for(auto it = puzzleData.back().crbegin(); it != puzzleData.back().crend(); it++)
            _edges[2].push_back(static_cast<TILE>(*it));
        
        for(const std::string& puzzleLine: puzzleData)
        {
            _edges[1].push_back(static_cast<TILE>(puzzleLine.back()));
            _edges[3].push_back(static_cast<TILE>(puzzleLine.front()));
        }       
        std::reverse(_edges[3].begin(), _edges[3].end());
    }
    
    void NexOrientation()
    {
        if(++_orientation == EDES_SIZE)
            _orientation = 0;
    }
    
    void Flip()
    {
        _isFlipped = !_isFlipped;
    }
    
    bool CompareEdges(int edgeIndex, const Puzzle& other, int otherEdgeIndex) const
    {
        int edgeIndexMapped{getEdgeIndex(edgeIndex)};
        int otherEdgeIndexMapped{getEdgeIndex(otherEdgeIndex)};
        
        if(_isFlipped == other._isFlipped)
        {
            return std::equal(_edges[edgeIndexMapped].begin(), _edges[edgeIndexMapped].end(), 
                              other._edges[otherEdgeIndexMapped].rbegin());
        }
        else
        {
             return std::equal(_edges[edgeIndexMapped].begin(), _edges[edgeIndexMapped].end(), 
                               other._edges[otherEdgeIndexMapped].cbegin());           
        }
    }
    
    bool IsEmpty() const
    {
        return _id == 0;
    }
    
    bool IsFlipped() const
    {
        return _isFlipped;
    }

    size_t Orientation() const
    {
        return _orientation;
    }
    
    void print() const
    {
        std::cout << _id << std::endl;
        for(const auto& edge : _edges)
        {
            for(const auto& tile : edge)
                std::cout << static_cast<char>(tile);
            std::cout << std::endl;
        }
    }
    
private:
    int getEdgeIndex(int index) const
    {
        size_t orientationIndex{(_orientation + index) % EDES_SIZE};
        if(!_isFlipped)
        {
            return orientationIndex;
        }
        else
        {
            static int flipMapping[]{0, 3, 2, 1};
            return flipMapping[orientationIndex];
        }
    }
    
    std::vector<Edge> _edges{EDES_SIZE};
    int _id;
    bool _isFlipped{false};
    unsigned _orientation{0};
};

class PuzzleTransformer
{
public:
    PuzzleTransformer(Puzzle & puzzle) : _puzzle(puzzle)
    {
    }
    
    bool Next()
    {
        if(++_state % FLIP_STATE)
            _puzzle.Flip();
        _puzzle.NexOrientation();
        
        if(_state == END_STATE)
            return false;                                       
        return true;
    }
    
    const Puzzle & TransformedPuzzle() const
    {
        return _puzzle;
    }
private:
    static const int END_STATE = 8;
    static const int FLIP_STATE = 4;
           
    Puzzle & _puzzle;
    int _state{0};
};

class PuzzleSolver
{
public:
    PuzzleSolver(const std::vector<Puzzle>& puzzles) : _puzzles(puzzles)
    {
    }
    
    void Solve()
    {
        initSolver();
        while(!_puzzleToProcess.empty())
        {
            Puzzle * puzzle = _puzzleToProcess.front();
            _puzzleToProcess.pop();
            bool matchFound{false};
                
            static size_t matchPairs[4] = { 2 , 3, 0, 1 };
            for(const auto& [point, edgeInfos] : _avaliableEdges)
            {
                PuzzleTransformer puzzleTransformer(*puzzle);
                do
                {
                    for(const auto& edgeInfo: edgeInfos)
                    {
                        if(edgeInfo.puzzle->CompareEdges(edgeInfo.index, puzzleTransformer.TransformedPuzzle(), matchPairs[edgeInfo.index]))
                        {
                            matchFound = true;
                            Point matchPoint{point};
                            _result[matchPoint] = puzzle;
                            switch(edgeInfo.index)
                            {
                                case 0:
                                    _avaliableEdges[{matchPoint.X-1, matchPoint.Y}].emplace_back(puzzle, 3);
                                    _avaliableEdges[{matchPoint.X, matchPoint.Y+1}].emplace_back(puzzle, 0);
                                    _avaliableEdges[{matchPoint.X+1, matchPoint.Y}].emplace_back(puzzle, 1);
                                    break;
                                case 1:
                                    _avaliableEdges[{matchPoint.X, matchPoint.Y+1}].emplace_back(puzzle, 0);
                                    _avaliableEdges[{matchPoint.X+1, matchPoint.Y}].emplace_back(puzzle, 1);
                                    _avaliableEdges[{matchPoint.X, matchPoint.Y-1}].emplace_back(puzzle, 2);
                                    break;
                                case 2:
                                    _avaliableEdges[{matchPoint.X+1, matchPoint.Y}].emplace_back(puzzle, 1);
                                    _avaliableEdges[{matchPoint.X, matchPoint.Y-1}].emplace_back(puzzle, 2);
                                    _avaliableEdges[{matchPoint.X-1, matchPoint.Y}].emplace_back(puzzle, 3);                                    
                                    break;
                                case 3:
                                    _avaliableEdges[{matchPoint.X, matchPoint.Y-1}].emplace_back(puzzle, 2);
                                    _avaliableEdges[{matchPoint.X-1, matchPoint.Y}].emplace_back(puzzle, 3);
                                    _avaliableEdges[{matchPoint.X, matchPoint.Y+1}].emplace_back(puzzle, 0);                                    
                                    break;                                    
                            }
                            _avaliableEdges.erase(matchPoint);
                            break;
                        }
                    }
                    if(matchFound)
                        break;
                }
                while(puzzleTransformer.Next());
                
                if(matchFound)
                    break;
            }
            if(!matchFound)
            {
                _puzzleToProcess.push(puzzle);
            }
        }
    }
    
private:
    struct Point
    {
        int X{0};
        int Y{0};
        
        bool operator<(const Point& other) const
        {
            if(X != other.X)
                return X < other.X;
            return Y < other.Y;
        }
    };
    
    struct EdgeInfo
    {
        const Puzzle* puzzle;
        size_t index;
        
        EdgeInfo(const Puzzle* puzz, size_t idx) : puzzle(puzz), index(idx)
        {
        }
    };
    
    using PuzzleMap = std::map<Point, const Puzzle*>;    
    using EdgeInfos = std::list<EdgeInfo>;
    using AvaliableEdges = std::map<Point, EdgeInfos>;
    using PuzzleToProcess = std::queue<Puzzle*>;
    
    void initSolver()
    {
        _puzzleToProcess = PuzzleToProcess();
        for(Puzzle& puzzle : _puzzles)
            _puzzleToProcess.push(&puzzle);
        
        _result.clear();
        _result[{0,0}] = _puzzleToProcess.front();   
        
        _avaliableEdges.clear();
        _avaliableEdges[{0,1}].emplace_back(_puzzleToProcess.front(), 0);
        _avaliableEdges[{1,0}].emplace_back(_puzzleToProcess.front(), 1);
        _avaliableEdges[{0,-1}].emplace_back(_puzzleToProcess.front(), 2);
        _avaliableEdges[{-1,0}].emplace_back(_puzzleToProcess.front(), 3);
        
        _puzzleToProcess.pop();   
    }
    
    PuzzleMap _result;
    AvaliableEdges _avaliableEdges;
    PuzzleToProcess _puzzleToProcess;
    
    std::vector<Puzzle> _puzzles;
};

int main(void)
{
    std::vector<Puzzle> puzzles;
    
    const std::regex tileRegex(R"***(Tile (\d+):)***");
    std::string input;
    while(std::getline(std::cin, input))
    {
        std::smatch matches;
        if (std::regex_match(input, matches, tileRegex))
        {
            int id{std::stoi(matches[1].str())};
            std::vector<std::string> puzzleData(PUZZLE_SIZE);
            int index{0};
            while(std::getline(std::cin, input) &&
                  !input.empty())
            {
                puzzleData[index++].swap(input);
            }
            puzzles.emplace_back(id, puzzleData);
            puzzles.back().print();
        }
    }
    
    PuzzleSolver solver(puzzles);
    solver.Solve();
    
    return 0;
}