#include <algorithm>
#include <iostream>
#include <deque>
#include <vector>
#include <set>

int main(void)
{
    std::vector<long> codesInput;
    const int preambleSize = 25;
    std::deque<long> codes;
    int input;
    long result1{-1};
    while(std::cin >> input)
    {
        codesInput.push_back(input);
        if(result1 != -1)
            continue;
        if(codes.size() != preambleSize)
        {
            codes.push_back(input);
        }
        else
        {
            bool numberValid{false};
            for(const auto& code : codes)
            {
                long temp = input - code;
                if(temp > 0 && 
                   std::find(codes.begin(), codes.end(), temp) != codes.end())
                {
                    numberValid = true;
                    break;
                }
            }
            if(numberValid)
            {
                codes.pop_front();
                codes.push_back(input);
            }
            else
            {
                result1 = input;
            }
        }
    }
    int result2{-1};
    for(auto it = codesInput.begin(); it != codesInput.end(); it++)
    {
        auto subIt = it;
        long temp = result1;
        do
        {
            temp -= *subIt++;
        }
        while(temp > 0 && subIt !=codesInput.end());
        if(temp == 0)
        {
            std::set<long> weakness(it, subIt);
            result2 = *weakness.begin() + *weakness.rbegin();
            break;
        }
    }
    std::cout << result1 << std::endl; //Task 1
    std::cout << result2 << std::endl; //Task 2
    return 0;
}