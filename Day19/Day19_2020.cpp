#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <boost/regex.hpp>

class Rules
{
	using RuleIndex = int;
	using RuleIndexList = std::list<RuleIndex>;
	using RulesIndexList = std::list<RuleIndexList>;
	using RulesMapUnresolved = std::map<RuleIndex, RulesIndexList>;
	using RulesMapProcessed = std::map<RuleIndex, std::string>;
public:
	bool MatchRule(const std::string& input) const
	{
		return boost::regex_match(input, _rule);
	}

	void InsertRule(const std::string& input)
	{
		RuleIndex ruleNumber;
		char dummy;
		std::istringstream in(input);
		in >> ruleNumber >> dummy >> std::ws;
		RulesIndexList rule;
		RuleIndexList subRule;
		while (!in.eof() || !subRule.empty())
		{
			RuleIndex temp;
			if ((in.eof() && !subRule.empty()) ||
				in.peek() == '|')
			{
				in.get();
				rule.push_back(subRule);
				subRule.clear();
			}
			else if (in.peek() == '\"')
			{
				in.get();
				in >> dummy;
				_ruleMapProcessedInit[ruleNumber] = dummy;
				return;
			}
			else if (in >> temp)
			{
				subRule.push_back(temp);
				in >> std::ws;
			}
		}
		_ruleMapToProcess[ruleNumber] = rule;
	}

	void ProcessRules(bool task2Exception = false)
	{
        _ruleMapProcessed = _ruleMapProcessedInit;
		for (const auto&[ruleNumber, rulesIndexList] : _ruleMapToProcess)
		{
			processRules(ruleNumber, rulesIndexList, task2Exception);
		}
        if(task2Exception)
        {
            int n=0;
            auto it = _ruleMapProcessed[0].begin();
            while(*it != '*')
            {
                if(*(it++) == '(')
                {
                    n++;
                }
            }
            _ruleMapProcessed[0].erase(it);
            size_t i = _ruleMapProcessed[0].find("n");
            _ruleMapProcessed[0].erase(i, 1);
            _ruleMapProcessed[0].insert(i, std::to_string(n+1));
        }
        _rule = boost::regex("^" + _ruleMapProcessed[0] + "$");
	}
    
private:
	std::string processRules(const RuleIndex ruleIndex, const RulesIndexList& rulesIndexList, bool task2Exception)
	{
		std::ostringstream out;
		size_t rulesIndexListIndex{ 0 };
		if (rulesIndexList.size() != 1)
			out << "(";
		for (const auto& ruleIndexList : rulesIndexList)
		{
			for (const auto& ruleIndex : ruleIndexList)
			{
				out << processRule(ruleIndex, task2Exception);
			}
			if (++rulesIndexListIndex != rulesIndexList.size())
				out << "|";
		}
		if (rulesIndexList.size() != 1)
			out << ")";
        
        std::string ruleStr;
        if(!task2Exception ||
           !(ruleIndex == 8 || ruleIndex == 11))
        {
            ruleStr = out.str();
        }
        else if(ruleIndex == 8)
        {
            ruleStr = "(" + _ruleMapProcessed.at(42) + ")+";
        }
        else if(ruleIndex == 11)
        {
            ruleStr = "*(" + _ruleMapProcessed.at(42) + "(?n)?" + _ruleMapProcessed.at(31) + ")";
        }
		return _ruleMapProcessed[ruleIndex] = ruleStr;
	}

	std::string processRule(const RuleIndex ruleIndex, bool task2Exception)
	{
		std::string result;
		if (_ruleMapProcessed.find(ruleIndex) != _ruleMapProcessed.end())
		{
			return _ruleMapProcessed.at(ruleIndex);
		}
		return processRules(ruleIndex, _ruleMapToProcess.at(ruleIndex), task2Exception);
	}

	RulesMapUnresolved _ruleMapToProcess;
    RulesMapProcessed _ruleMapProcessedInit;
    
	RulesMapProcessed _ruleMapProcessed;
	boost::regex _rule;
};

int main(void)
{
	Rules rules;
    std::list<std::string> inputs;
	int result1{ 0 };

	std::string input;
	bool rulesProcessing{ true };
	while (std::getline(std::cin, input))
	{
		if (input.empty())
		{
			rulesProcessing = false;
			rules.ProcessRules();
		}
		if (rulesProcessing)
		{
			rules.InsertRule(input);
		}
		else
		{
            inputs.push_back(input);
			if (rules.MatchRule(input))
			{
				result1++;                
			}
		}
	}
	std::cout << result1 << std::endl;

    int result2{0};
    rules.ProcessRules(true);
    for(const auto& input : inputs)
    {
		if (rules.MatchRule(input))
        {
            result2++;
        }
    }
    std::cout << result2 << std::endl;
    
	return 0;
}