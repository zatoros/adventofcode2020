#include <iostream>
#include <map>
#include <string>
#include <utility>

class TreeMap
{
public:
	using Input = std::map<int, std::map<int, bool> >;

	TreeMap(Input input) : _input(input) {}

	unsigned long long CountResult(int x, int y)
	{
		unsigned long result = 0;
		std::pair<int, int> pos(0, 0);
		while (static_cast<size_t>(pos.first += x) < _input.size())
		{
			pos.second += y;
			if (isTree(pos.first, pos.second))
				result++;
		}
		return result;
	}

private:
	bool isTree(int line, int column) const
	{
		return _input.at(line).at(column % _input.at(0).size());
	}
	Input _input;
};

int main()
{
	TreeMap::Input treeMapInput;
	std::string input;
	int line = 0, column;
	while (std::getline(std::cin, input))
	{
		column = 0;
		for (const char mapSymbol : input)
		{
			treeMapInput[line][column++] = (mapSymbol == '#');
		}
		line++;
	}

	TreeMap treeMap(treeMapInput);

	std::cout << treeMap.CountResult(1, 3) << std::endl;
	std::cout << treeMap.CountResult(1, 1) * 
		         treeMap.CountResult(1, 3) *
		         treeMap.CountResult(1, 5) *
		         treeMap.CountResult(1, 7) * 
		         treeMap.CountResult(2, 1) << std::endl;
}