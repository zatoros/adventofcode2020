#include <iostream>
#include <string>
#include <regex>
#include <algorithm>

class Password
{
	int minOccur;
	int maxOccur;
	char symbol;
	std::string password;

public:

	Password(const std::string& input)
	{
		static const std::regex inputPattern(R"***(^(\d+)-(\d+) (\w): (\w+)$)***");
		static std::smatch matches;

		if (std::regex_search(input, matches, inputPattern) && 
			matches.size() == 5)
		{
			minOccur = std::stoi(matches[1].str());
			maxOccur = std::stoi(matches[2].str());
			symbol = matches[3].str()[0];
			password = matches[4].str();
		}
	}

	bool Verify1()
	{
		int count = std::count(password.begin(), password.end(), symbol);
		return count >= minOccur && count <= maxOccur;
	}

	bool Verify2()
	{
		try
		{
			return (password.at(minOccur - 1) == symbol) !=
				(password.at(maxOccur - 1) == symbol);
		}
		catch (std::out_of_range&)
		{
			return false;
		}
	}
};

int main()
{
	int result1 = 0, result2 = 0;
	std::string input;
	std::smatch matches;
	while (std::getline(std::cin, input))
	{
		Password password(input);
		if (password.Verify1())
		{
			result1++;
		}
		if (password.Verify2())
		{
			result2++;
		}
	}

	std::cout << result1 << std::endl;
	std::cout << result2 << std::endl;
}