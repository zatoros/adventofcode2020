#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <set>
#include <vector>

int main(void)
{
    int resultSum{0}, resultIntersect{0};
    std::string seatInput;
    std::set<char> yesAnswersSum, yesAnswersIntersect;
    bool newGroup{true}, lastLine{false};
    do
    {
        if(!std::getline(std::cin, seatInput))
        {
            lastLine = true;
        }
        if(seatInput.empty() || lastLine)
        {
            resultSum += yesAnswersSum.size();
            yesAnswersSum.clear();
            
            resultIntersect += yesAnswersIntersect.size();
            yesAnswersIntersect.clear();
            newGroup = true;
        }
        else
        {
            yesAnswersSum.insert(seatInput.begin(), seatInput.end());
            
            if(newGroup)
            {
                yesAnswersIntersect.insert(seatInput.begin(), seatInput.end());
                newGroup = false;
            }
            else
            {
                std::set<char> intersect;
                std::sort(seatInput.begin(), seatInput.end());
                std::set_intersection(yesAnswersIntersect.begin(), yesAnswersIntersect.end(),
                                      seatInput.begin(), seatInput.end(),
                                      std::insert_iterator<std::set<char>>(intersect, intersect.begin()));
                yesAnswersIntersect.swap(intersect);
            }
        }
    }
    while(!lastLine);
    
    std::cout << resultSum << std::endl; //part 1
    std::cout << resultIntersect << std::endl; //part 2
    return 0;
}