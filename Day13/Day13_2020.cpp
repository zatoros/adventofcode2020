#include <cmath>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>

bool isGoldenValue(const std::vector<std::pair<unsigned, unsigned>>& busTable, unsigned long long value)
{
    for(const auto&[id, delay] : busTable)
    {
        if(value % id != delay)
            return false;
    }
    return true;
}

int main(void)
{
	unsigned busTime;
    std::cin >> busTime;
    
    std::string input;
    std::getline(std::cin, input); // empty line
    std::getline(std::cin, input);
    
    std::vector<std::string> busTimesStr;
    boost::split(busTimesStr, input, boost::is_any_of(","));
    
    std::vector<unsigned> busTimes;
    for(const auto& inStr : busTimesStr)
    {
        if(inStr != "x")
        {
            busTimes.push_back(std::stoi(inStr));
        }
    }
    
    std::map<unsigned, unsigned> results;
    for(const auto& busId : busTimes)
    {
        results[busId - (busTime % busId)] = busId;
    }    
    std::cout << results.begin()->first * results.begin()->second << std::endl; // task 1
    
    std::vector<std::pair<unsigned, unsigned>> busTable;
    int delay{0};
    for(const auto& inStr : busTimesStr)
    {
        if(inStr != "x")
        {
			int id = std::stoi(inStr);
			busTable.push_back({ id, id - (delay % id) });
        }
        delay++;
    }
    
	auto it = busTable.begin();
	unsigned long long goldenValue{ it->second };
	unsigned long long step{ it->first };
	while (++it != busTable.end())
	{
		unsigned long long firstMatch{ 0 };
		while (true)
		{
			if (goldenValue % it->first == it->second)
			{
				if (firstMatch == 0)
				{
					firstMatch = goldenValue;
					if (it + 1 == busTable.end())
					{
						break;
					}
				}
				else
				{
					step = goldenValue - firstMatch;
					goldenValue = firstMatch;
					break;
				}
			}
			goldenValue += step;
		}
	}
    std::cout << goldenValue << std::endl;
    
    return 0;
}