#include <cctype>
#include <iostream>
#include <list>
#include <sstream>
#include <string>

using resultType = unsigned long long;

enum Operator : char
{
	ADD = '+',
	MUL = '*',
	EMPTY = ' '
};

resultType calcExpression(std::istringstream& in);
resultType calcWithPrecedence(std::istringstream& in);

bool calcArg(std::istream& in, resultType& arg, bool withPrecedence = false)
{
	in >> std::ws;
	if (in.eof())
		return false;
	if (std::isdigit(in.peek()))
	{
		in >> arg;
		return true;
	}
	else if (in.peek() == '(')
	{
		in.get();
		int breacketRefCount{ 1 };
		std::ostringstream out;
		while (true)
		{
			if (in.peek() == '(')
				breacketRefCount++;
			else if (in.peek() == ')')
				breacketRefCount--;
			if (breacketRefCount == 0)
			{
				in.get();
				std::istringstream subIn(out.str());
				if (!withPrecedence)
				{
					arg = calcExpression(subIn);
				}
				else
				{
					arg = calcWithPrecedence(subIn);
				}
				return true;
			}
			out << static_cast<char>(in.get());
		}
	}
	return false;
}

bool calcOp(std::istream& in, Operator& arg)
{
	in >> std::ws;
	if (in.eof())
		return false;
	arg = static_cast<Operator>(in.get());
	return true;
}

resultType calcExpression(std::istringstream& in)
{
	resultType result, arg;
	Operator op;

	calcArg(in, result);
	while (calcOp(in, op))
	{
		calcArg(in, arg);
		if (op == ADD)
			result += arg;
		else if (op == MUL)
			result *= arg;
	}
	return result;
}

resultType calcWithPrecedence(std::istringstream& in)
{
	resultType result{ 0 };
	std::list<resultType> args;
	std::list<Operator> ops;
	resultType arg;
	Operator op;

	calcArg(in, arg, true);
	args.push_back(arg);
	while (calcOp(in, op))
	{
		calcArg(in, arg, true);
		args.push_back(arg);
		ops.push_back(op);
	}
	auto arg2It = args.begin();
	auto arg1It = arg2It++;
	auto opIt = ops.begin();
	while (arg2It != args.end())
	{
		if (*opIt == ADD)
		{
			opIt = ops.erase(opIt);
			*arg1It = *arg1It + *arg2It;
			arg2It = args.erase(arg2It);
		}
		else
		{
			opIt++;
			arg1It++;
			arg2It++;
		}
	}
	auto argIt = args.begin();
	result = *(argIt++);
	while (argIt != args.end())
	{
		result *= *(argIt++);
	}
	return result;
}

int main()
{
	resultType result1{ 0 }, result2{ 0 };
	std::string input;
	while (std::getline(std::cin, input))
	{
		std::istringstream in1(input), in2(input);
		result1 += calcExpression(in1);
		result2 += calcWithPrecedence(in2);
	}
	std::cout << result1 << std::endl;
	std::cout << result2 << std::endl;
}