#include <iostream>
#include <set>
#include <map>

long factorial(int n)
{
    long result = 1;
    for(int i = 2; i <=n; ++i)
    {
        result *= i;
    }
    return result;
}

int calcPermutations(int length, int ones = 0, int twoes = 0, int threes = 0)
{
    return factorial(length)/factorial(ones)/factorial(twoes)/factorial(threes);
}

long calcComb2(int length)
{
    int ones{length}, twoes{0};
    long result{0};
    while( (ones -= 2) >= 0 )
    {
        twoes +=1;
        result += calcPermutations(ones + twoes, ones, twoes);
    }
    return result;
}

long calcComb3(int length)
{
    int ones{length - 3}, twoes{0}, threes{1};
    long result{0};
    while(ones >= 0)
    {
        result += calcPermutations(ones + twoes + threes, ones, twoes, threes);
        ones -= 2;
        twoes += 1;
        if(ones < 0)
            break;
        result += calcPermutations(ones + twoes + threes, ones, twoes, threes);
        ones -= 1;
        twoes -= 1;
        threes += 1;
    }
    return result;
}

long calcComb(int length)
{
    if(length <= 1)
        return 1;
    
    return 1 + calcComb2(length) + calcComb3(length);
}

long calcCombination(int length)
{
    static std::map<int, long> cache;
    
    auto it = cache.find(length);
    if(it != cache.end())
        return it->second;
    
    cache[length] = calcComb(length);
    return cache[length];
}

int main(void)
{
    std::set<int> adapters;
    for(int input; std::cin >> input;)
    {
        adapters.insert(input);
    }
    adapters.insert(0);
    adapters.insert(*adapters.rbegin() + 3);
    
    int result1{0};
    {
        auto it1 = adapters.begin();
        auto it2 = adapters.begin();
        int diff1{0}, diff3{0};
        it2++;
        while(it2 != adapters.end())
        {
            int diff = *it2 - *it1;
            if(diff == 1)
                diff1++;
            else if(diff ==3)
                diff3++;
            it1++;
            it2++;
        }
        result1 = diff1 * diff3;
    }
    long result2{1};
    {
        auto it1 = adapters.begin();
        auto it2 = adapters.begin();
        it2++;
        int diff1{0};
        while(it2 != adapters.end())
        {
            int diff = *it2 - *it1;
            if(diff == 1)
            {
                diff1++;
            }
            else if(diff == 3)
            {
                result2 *= calcCombination(diff1);
                diff1 = 0;
            }
            it1++;
            it2++;
        }
    }
    
    std::cout << result1 << std::endl; // Task 1
    std::cout << result2 << std::endl; // Task 2
}