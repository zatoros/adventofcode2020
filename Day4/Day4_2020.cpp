#include <iostream>
#include <string>
#include <regex>
#include <map>
#include <vector>
#include <algorithm>

class Passport
{
public:
	Passport(const std::string& input)
	{
		static const std::regex inputPattern(R"***((\S+):(\S+) )***");
		static std::smatch matches;

		std::string temp;
		for (auto it = std::sregex_token_iterator(input.begin(), input.end(), inputPattern, { 1, 2 });
			it != std::sregex_token_iterator(); it++)
		{
			if (!temp.empty())
			{
				fields[temp] = *it;
				temp.clear();
			}
			else
			{
				temp = *it;
			}
		}
	}

	bool Verify() const
	{
		using RequiredField = std::pair<std::string, bool(*)(const std::string&)>;
		static const std::vector<RequiredField> requiredFields
		{ {"byr", validateByr},
		  {"iyr", validateIyr},
		  {"eyr", validateEyr},
		  {"hgt", validateHgt},
		  {"hcl", validateHcl},
		  {"ecl", validateEcl},
		  {"pid", validatePid} };

		return std::all_of(requiredFields.begin(), requiredFields.end(),
			[this](const RequiredField& requiredField) { return this->fields.find(requiredField.first) != this->fields.end() &&
			requiredField.second(this->fields.at(requiredField.first)); });
	}

	static bool validateNumber(const std::string& field, int min, int max)
	{
		try
		{
			int number = std::stoi(field);
			return number >= min && number <= max;
		}
		catch (...)
		{
			return false;
		}
	}

	static bool validatePattern(const std::string& field, const std::string& pattern)
	{
		std::regex fieldPattern(pattern);
		static std::smatch match;
		return std::regex_match(field, match, fieldPattern);
	}

	static bool validateByr(const std::string& field)
	{
		return validateNumber(field, 1920, 2002);
	}

	static bool validateIyr(const std::string& field)
	{
		return validateNumber(field, 2010, 2020);
	}

	static bool validateEyr(const std::string& field)
	{
		return validateNumber(field, 2020, 2030);
	}

	static bool validateHgt(const std::string& field)
	{
		std::regex fieldPattern("(\\d+)(cm|in)");
		static std::smatch match;
		if (std::regex_match(field, match, fieldPattern))
		{
			if (match.size() == 3)
			{
				if (match[2].str() == "cm")
				{
					return validateNumber(match[1].str(), 150, 193);
				}
				else if (match[2].str() == "in")
				{
					return validateNumber(match[1].str(), 59, 76);
				}
			}
		}
		return false;
	}

	static bool validateHcl(const std::string& field)
	{
		return validatePattern(field, "#[0-9a-f]{6}");
	}

	static bool validateEcl(const std::string& field)
	{
		return validatePattern(field, "amb|blu|brn|gry|grn|hzl|oth");
	}

	static bool validatePid(const std::string& field)
	{
		return validatePattern(field, "\\d{9}");
	}

private:
	std::map<std::string, std::string> fields;
};

int main()
{
	int result = 0;
	std::string input;
	std::string inputRecord;
	while (std::getline(std::cin, input) || !inputRecord.empty())
	{
		if (input.empty())
		{
			if (Passport(inputRecord).Verify())
			{
				result++;
			}
			inputRecord.clear();
		}
		else
		{
			inputRecord.append(input).append(" ");
		}
	}

	std::cout << result << std::endl;
}