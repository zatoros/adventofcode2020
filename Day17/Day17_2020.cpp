#include <algorithm>
#include <initializer_list>		
#include <iostream>
#include <list>
#include <map>
#include <string>
#include <set>
#include <tuple>
#include <vector>

enum State : char
{
    ACTIVE = '#',
    INACTIVE = '.'
};

class Point
{
public:
    static constexpr int DIM{4};
    
    static const std::vector<Point>& GetCube() 
    {
        return cubeGenerator.GetCube();
    }
    
    Point() = default;
    Point(std::initializer_list<int> list)
    {
        int index{0};
        for(const auto& value : list)
        {
            _cords[index++] = value;
            if(index == DIM)
                break;
        }
    }
    
    template<typename It>
    Point(It firstIt, It lastIt)
    {
        int index{0};
        while(firstIt != lastIt)
        {
            _cords[index++] = *firstIt++;
            if(index == DIM)
                break;
        }
    }
    
    Point operator+(const Point& other) const
    {
        Point result;
        std::transform(_cords, _cords + DIM, 
                       other._cords, result._cords, 
                       std::plus<int>());
        return result;
    }
    
    bool operator<(const Point& other) const
    {
        return 
            std::lexicographical_compare(_cords, _cords + DIM,
                                         other._cords, other._cords + DIM);
    }
    
private:    
    class CubeGenerator
    {
    public:
        CubeGenerator(int dim)
        {
            std::list<int> data;
            generate(dim, data);
        }
        
        const std::vector<Point>& GetCube() const
        {
            return _qube;
        }
        
    private:
        void generate(int dim, std::list<int>& data)
        {            
            if(dim == static_cast<int>(data.size()))
            {
                _qube.push_back(Point(data.begin(), data.end()));
                return;
            }
            for(int i = -1; i <= 1; i++)
            {
                data.push_back(i);
                generate(dim, data);
                data.pop_back();
            }
        }
        std::vector<Point> _qube;
    };
    
    static CubeGenerator cubeGenerator;
    
    int _cords[DIM];
};

Point::CubeGenerator Point::cubeGenerator(Point::DIM);

using Map = std::map<Point, bool>;

class MapProcessor
{
public:
    MapProcessor(const Map& map) : _map(map)
    { }
    
    void Execute(int steps)
    {
        while(steps--)
        {
            std::set<Point> points = 
                getPointsToProcess();
            makeStep(points);
        }
        filterMap();
    }
    
    int GetActiveCubes() const
    {
        return _map.size();
    }
private:
    std::set<Point> getPointsToProcess() const
    {
        std::set<Point> points;
        for(const auto& [point, isActive] : _map)
        {
            if(!isActive)
                continue;
            for(const auto& cubePoint : Point::GetCube())
            {
                points.insert(point + cubePoint);
            }
        }
        return points;
    }
    
    void makeStep(const std::set<Point>& points)
    {
        Map newMap;
        for(const auto& point : points)
        {
            int activeNeighbours = countActiveNeighbours(point);
            if(_map[point])
            {
                if(activeNeighbours == 2 || activeNeighbours == 3)
                {
                    newMap[point] = true;
                }
            }
            else if (activeNeighbours == 3)
            {
                newMap[point] = true;
            }
        }
        _map.swap(newMap);
    }
    
    int countActiveNeighbours(const Point& point)
    {
        int result = (_map[point] ? -1 : 0);
        for(const auto& cubePoint : Point::GetCube())
        {
            if(_map[point + cubePoint])
                result++;
        }
        
        return result;
    }
    
    void filterMap()
    {
        for(auto mapIt = _map.begin(); mapIt != _map.end();)
        {
            if(mapIt->second)
            {
                mapIt++;
            }
            else
            {
                mapIt = _map.erase(mapIt);
            }
        }
    }
    
    Map _map;
};

int main(void)
{
    Map map;
    
    std::string input;
    int x{0};
    while(std::getline(std::cin, input))
    {
        for(int y = 0; y < static_cast<int>(input.size()); y++)
        {
            if(input[y] == State::ACTIVE)
            {
                map[{x,y}] = true;
            }
        }
        x++;
    }
    
    MapProcessor mapProcessor(map);
    mapProcessor.Execute(6);
    std::cout << mapProcessor.GetActiveCubes() << std::endl;
    return 0;
}